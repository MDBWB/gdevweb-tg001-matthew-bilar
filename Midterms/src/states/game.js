class Game extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.image('dvd', 'assets/dvd.png');
    }

    create() {
        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();
        
        this.dvd = this.game.world.addChild (new Dvd (this.game, this.game.world.centerX, this.game.world.centerY, 'dvd'));
        
        this.input.onDown.add(function () {
            this.game.state.start('menu');
        }, this);
    }

    
}
