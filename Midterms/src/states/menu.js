class Menu extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        // fit to screen
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
    }

    create() {
        this.background = this.game.add.graphics();
        this.background.beginFill (0x000000);
        this.background.drawRect (0, 0, this.game.width, this.game.height);
        this.background.endFill ();
        

        var style = {
            font: 'Arial',
            fontSize: 32,
            fill: "#ffffff",
            align: "center"
        };
        
        this.titleText = this.game.add.text (this.game.world.centerX, this.game.height * 0.25, "Simple Screen Saver", style);
        this.titleText.anchor.setTo (0.5, 0.5);
        style = {
            font: 'Arial',
            fontSize: 24,
            fill: "#ffffff",
            align: "center"
        };

        this.instructionText = this.game.add.text (this.game.world.centerX, this.game.height * 0.5, "Touch me to start!", style);
        this.instructionText.anchor.setTo (0.5, 0.5);

        this.input.onDown.add(function () {
            this.game.state.start('game');
        }, this);
    }
}
