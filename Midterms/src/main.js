// set background color to black
window.document.body.style["background-color"] = "#333333";

// define phaser game
const game = new Phaser.Game(800, 800, Phaser.AUTO, window.gamebox); 
window.game = game;

// add menu state
game.state.add('menu', new Menu());

game.state.add('game', new Game());

// initialize menu state
game.state.start('menu');