class PlayerAnimation extends Phaser.Sprite {
    constructor(game, x, y, key) {
        super(game, x, y, key);
        
        this.animations.add ("left-walk", [0,1,2,3]);
        this.animations.add ("idle", [4]);
        this.animations.add ("right-walk", [5,6,7,8]);
        this.animations.play ("idle", 12, true);
    }

    changeAnim (anim) {
        this.animations.play (anim, 12, true);
    }

    update() {
        
    }
}
