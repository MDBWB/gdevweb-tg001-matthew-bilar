class RectCollision extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        
    }

    create() {
        this.background = this.game.add.image(0,0,'background');

        this.input.onDown.add(this._startMenu, this);

        this.text = this.game.add.text (this.game.width * 0.5, this.game.height * 0.1, "Overlapping: false");
        this.text.anchor.setTo (0.5, 0.5);
        
        this.otherRect = this.createRect (this.game.width * 0.75, this.game.height * 0.75);
        this.playerRect = this.createRect (this.game.width * 0.25, this.game.height * 0.25);
    }   

    createRect (x, y) {
        var graphics = this.game.add.graphics();

        graphics.beginFill(Math.random() * 0xffffff);
        graphics.drawRect (0, 0, 200, 100);
        graphics.endFill ();

        var sprite = this.game.add.sprite (x, y, graphics.generateTexture ());
        sprite.anchor.setTo (0.5, 0.5);

        graphics.destroy ();

        return sprite;
    }

    update() {
        // read keyboard input
        this.checkInput ();
        
        // check if two sprites overlapped
        if (this.checkOverlap(this.playerRect, this.otherRect))
        {
            this.text.text = 'Overlapped: true';
        }
        else
        {
            this.text.text = 'Overlapped: false';
        }
    }

    checkInput () {
        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT) ||
            game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            this.playerRect.x -= 5;
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) ||
                  game.input.keyboard.isDown(Phaser.Keyboard.D)){
            this.playerRect.x += 5;
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.UP) ||
            game.input.keyboard.isDown(Phaser.Keyboard.W)){
            this.playerRect.y -= 5;
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.DOWN) ||
            game.input.keyboard.isDown(Phaser.Keyboard.S)){
            this.playerRect.y += 5;
        }
    }

    checkOverlap (spriteA, spriteB) {

        var boundsA = spriteA.getBounds();
        var boundsB = spriteB.getBounds();
    
        return Phaser.Rectangle.intersects(boundsA, boundsB);    
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
