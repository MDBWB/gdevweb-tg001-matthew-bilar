class ColorTint extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.image('dota-logo', 'assets/dota-logo.png');
    }

    create() {
        this.background = this.game.add.image(this.game.width,this.game.height,'background');

        this.input.onDown.add(this._startMenu, this);

        this.createTint ();
    }   

    createTint () {
        var img = this.game.add.image(this.game.world.centerX, this.game.world.centerY, "dota-logo");
        img.anchor.setTo (0.5, 0.5);
        
        this.game.time.events.loop(Phaser.Timer.SECOND, function () {
            img.tint = Math.random() * 0xffffff;
        }, this);
    }

    update() {

    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
