class CircleCollision extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        
    }

    create() {
        this.background = this.game.add.image(0,0,'background');

        this.input.onDown.add(this._startMenu, this);

        this.text = this.game.add.text (this.game.width * 0.5, this.game.height * 0.1, "Overlapping: false");
        this.text.anchor.setTo (0.5, 0.5);
        
        this.otherCircle = this.createRect (this.game.width * 0.5, this.game.height * 0.5, 200);
        this.playerCircle = this.createRect (this.game.width * 0.25, this.game.height * 0.25, 100);
    }   

    createRect (x, y, radius) {
        var graphics = this.game.add.graphics();

        graphics.beginFill(Math.random() * 0xffffff);
        graphics.drawCircle (0, 0, radius);
        graphics.endFill ();

        var sprite = this.game.add.sprite (x, y, graphics.generateTexture ());
        sprite.anchor.setTo (0.5, 0.5);
        sprite.radius = radius * 0.5

        graphics.destroy ();

        return sprite;
    }

    update() {
        // read keyboard input
        this.checkInput ();
        
        // check if two sprites overlapped
        if (this.checkOverlap(this.playerCircle, this.otherCircle))
        {
            this.text.text = 'Overlapped: true';
        }
        else
        {
            this.text.text = 'Overlapped: false';
        }
    }

    checkInput () {
        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT) ||
            game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            this.playerCircle.x -= 5;
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) ||
                  game.input.keyboard.isDown(Phaser.Keyboard.D)){
            this.playerCircle.x += 5;
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.UP) ||
            game.input.keyboard.isDown(Phaser.Keyboard.W)){
            this.playerCircle.y -= 5;
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.DOWN) ||
            game.input.keyboard.isDown(Phaser.Keyboard.S)){
            this.playerCircle.y += 5;
        }
    }

    checkOverlap (spriteA, spriteB) {
        var dist = spriteA.radius + spriteB.radius;
        var dx = spriteA.x - spriteB.x;
        var dy = spriteA.y - spriteB.y;
        
        return dist * dist > (dx * dx + dy * dy);
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
