class Midterms extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.image('NvS', 'assets/NvS.jpg');
    }

    create() {
        console.log("Game!");
        
        this.background = this.game.add.image(0,0,'NvS');

        this.input.onDown.add(this._startMenu, this);
    }

    update() {
        
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
