class BitmapText extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        game.load.bitmapFont('desyrel', 'assets/desyrel.png', 'assets/desyrel.xml');
    }

    create() {
        this.background = this.game.add.image(0,0,'background');

        this.input.onDown.add(this._startMenu, this);

        this.text = this.createBitmapText ();
        this.text.position.setTo (this.game.world.centerX, this.game.world.centerY);
        this.text.anchor.setTo (0.5, 0.5);
        this.text.align = "center";
        
        this.counter = 0;
        this.game.time.events.loop(100, this.addChar, this);
    }   

    createBitmapText () {
        return game.add.bitmapText(0, 0, 'desyrel', "This is a bitmap text.", 64);
    }

    addChar () {
        this.text.text += Math.random().toString(36).substring(7);
        if (++this.counter % 2 == 0) {
            this.text.text += "\n";
        }
    }

    update() {
        
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
