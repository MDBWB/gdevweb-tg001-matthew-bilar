class BasicText extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        
    }

    create() {
        this.background = this.game.add.image(0,0,'background');

        this.input.onDown.add(this._startMenu, this);

        this.text = this.createText ();
        this.text.position.setTo (this.game.world.centerX, this.game.world.centerY);
        this.text.anchor.setTo (0.5, 0.5);

        this.counter = 0;
        this.game.time.events.loop(100, this.addChar, this);
    }   

    createText () {
        var style = {
            fontSize: 60,
            font: "Arial",
            fill: "#000000",
            align: "left",
            fontWeight: "600",
            boundsAlignH: "left",
            boundsAlignV: "top",
            stroke: "#ffffff",
            strokeThickness: 5,
            wordWrap: true,
            wordWrapWidth: this.game.width * 0.5
        };

        return this.game.add.text (this.game.width * 0.5, this.game.height * 0.1, "This is a basic text.", style);
    }

    addChar () {
        this.text.text += Math.random().toString(36).substring(7);
        if (++this.counter % 2 == 0) {
            this.text.text += "\n";
        }
    }

    update() {
        
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
