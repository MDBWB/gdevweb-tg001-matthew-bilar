class Game extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.spritesheet('dude', 'assets/dude.png', 32, 48, 9);
    }

    create() {
        console.log("Game!");
        
        // add the background image at position (0,0)
        this.background = this.game.add.image(this.game.width,this.game.height,'background');

        // flip background by 180 degrees
        this.background.angle = 180;

        var player = this.game.add.sprite(0,0,"dude");
        player.animations.add("walk");
        player.animations.play("walk", 30, true);

        this.input.onDown.add(this._startMenu, this);
    }

    update() {
        
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
