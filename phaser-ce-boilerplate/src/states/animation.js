class Animation extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.spritesheet('dude', 'assets/dude.png', 32, 48, 9);
    }

    create() {        
        // add the background image at position (0,0)
        this.background = this.game.add.image(this.game.width,this.game.height,'background');

        // flip background by 180 degrees
        this.background.angle = 180;

        this.player = this.game.world.addChild (new PlayerAnimation (this.game, this.game.world.centerX, this.game.world.centerY, "dude"));

        this.input.onDown.add(this._startMenu, this);
    }

    update() {
        if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT) ||
            game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            this.player.x -= 2;
            this.player.changeAnim ("left-walk");
        } else if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) ||
                  game.input.keyboard.isDown(Phaser.Keyboard.D)){
            this.player.x += 2;
            this.player.changeAnim ("right-walk");
        } else {
            this.player.changeAnim ("idle");
        }
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
