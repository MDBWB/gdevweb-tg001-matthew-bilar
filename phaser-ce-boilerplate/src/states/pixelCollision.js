class PixelCollision extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        
    }

    create() {
        this.background = this.game.add.image(0,0,'background');

        this.input.onDown.add(this._startMenu, this);

        this.text = this.game.add.text (this.game.width * 0.5, this.game.height * 0.1, "Overlapping: false");
        this.text.anchor.setTo (0.5, 0.5);
        
        this.otherSprite = this.createRect (this.game.width * 0.5, this.game.height * 0.5);
    }   

    createRect (x, y, radius) {
        var graphics = this.game.add.graphics();

        // set a fill and line style
        graphics.beginFill(0xFF3300);
        graphics.lineStyle(10, 0xffd900, 1);
        
        // draw a shape
        graphics.moveTo(50,50);
        graphics.lineTo(250, 50);
        graphics.lineTo(100, 100);
        graphics.lineTo(250, 220);
        graphics.lineTo(50, 220);
        graphics.lineTo(50, 50);
        graphics.endFill();
        
        // set a fill and line style again
        graphics.lineStyle(10, 0xFF0000, 0.8);
        graphics.beginFill(0xFF700B, 1);
        
        // draw a second shape
        graphics.moveTo(210,300);
        graphics.lineTo(450,320);
        graphics.lineTo(570,350);
        graphics.quadraticCurveTo(600, 0, 480,100);
        graphics.lineTo(330,120);
        graphics.lineTo(410,200);
        graphics.lineTo(210,300);
        graphics.endFill();
        
        // draw a rectangle
        graphics.lineStyle(2, 0x0000FF, 1);
        graphics.drawRect(50, 250, 100, 100);
        
        // draw a circle
        graphics.lineStyle(0);
        graphics.beginFill(0xFFFF0B, 0.5);
        graphics.drawCircle(470, 200, 200);
        graphics.endFill();

        graphics.lineStyle(20, 0x33FF00);
        graphics.moveTo(30,30);
        graphics.lineTo(600, 300);

        var sprite = this.game.add.sprite (x, y, graphics.generateTexture ());
        sprite.anchor.setTo (0.5, 0.5);
        sprite.inputEnabled = true;
        sprite.input.pixelPerfectOver = true;
        
        graphics.destroy ();

        return sprite;
    }

    update() {
        // check if pointer overlapped sprite
        if (this.otherSprite.input.checkPointerOver(this.game.input))
        {
            this.text.text = 'Overlapped: true';
        }
        else
        {
            this.text.text = 'Overlapped: false';
        }
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
