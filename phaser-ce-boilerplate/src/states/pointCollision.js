class PointCollision extends Phaser.State {
    constructor() {
        super();
    }

    preload() {
        
    }

    create() {
        this.background = this.game.add.image(0,0,'background');

        this.input.onDown.add(this._startMenu, this);

        this.text = this.game.add.text (this.game.width * 0.5, this.game.height * 0.1, "Overlapping: false");
        this.text.anchor.setTo (0.5, 0.5);
        
        this.otherRect = this.createRect (this.game.width * 0.5, this.game.height * 0.5);
    }   

    createRect (x, y, radius) {
        var graphics = this.game.add.graphics();

        graphics.beginFill(Math.random() * 0xffffff);
        graphics.drawRect (0, 0, 300, 300);
        graphics.endFill ();

        var sprite = this.game.add.sprite (x, y, graphics.generateTexture ());
        sprite.anchor.setTo (0.5, 0.5);

        graphics.destroy ();

        return sprite;
    }

    update() {
        // check if pointer overlapped sprite
        if (this.checkOverlap(this.game.input, this.otherRect))
        {
            this.text.text = 'Overlapped: true';
        }
        else
        {
            this.text.text = 'Overlapped: false';
        }
    }

    checkOverlap (point, sprite) {
        return sprite.getBounds().contains(point.x, point.y)
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
