class Menu extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        // loading an image from the assets folder
        this.game.load.image('background', 'assets/background.png');
    }

    create() {
        console.log("Menu!");
        
        // add the background image at position (0,0)
        this.background = this.game.add.image(0,0,'background');
        
        
        this.createButton (this.game.width * 0.25, 70, "Intro", this.onButton1);
        this.createButton (this.game.width * 0.25, 130, "Player animation", this.onButton2);
        this.createButton (this.game.width * 0.25, 190, "Graphics", this.onButton3);
        this.createButton (this.game.width * 0.25, 250, "Masking", this.onButton4);
        this.createButton (this.game.width * 0.25, 310, "Tinting", this.onButton5);
        this.createButton (this.game.width * 0.25, 370, "Point Collision", this.onButton8);
        this.createButton (this.game.width * 0.25, 430, "Rect Collision", this.onButton6);
        this.createButton (this.game.width * 0.25, 490, "Circle Collision", this.onButton7);
        this.createButton (this.game.width * 0.25, 550, "Pixel-Perfect Collision", this.onButton9);
        this.createButton (this.game.width * 0.75, 70, "Basic Text", this.onButton10);
        this.createButton (this.game.width * 0.75, 130, "Bitmap Text", this.onButton11);   
        this.createButton (this.game.width * 0.75, 190, "Midterms", this.onButton12);   
    }

    createButton (x, y, label, onClick) {
        // create graphics as texture of button
        var btnImg = new Phaser.Graphics (this.game, 0, 0);
        btnImg.beginFill (0x0061ff);
        btnImg.drawRect (0, 0, 150, 50);
        btnImg.endFill ();

        // initialize font settings of button label
        var style = {
            font: 'Arial',
            fontSize: 16,
            fill: "#ffffff",
            align: "center"
        };

        // button label text
        var text = new Phaser.Text (this.game, 0, 0, label, style)
        text.anchor.setTo (0.5, 0.5);

        // button object
        var button = this.game.add.button (x, y, btnImg.generateTexture(), onClick);
        button.addChild (text);
        button.anchor.setTo (0.5, 0.5);

        return button;
    }

    onButton1 () {
        console.log ("pressed button 1");
        this.game.state.start('intro');
    }

    onButton2 () {
        console.log ("pressed button 2");
        this.game.state.start('animation');
    }

    onButton3 () {
        console.log ("pressed button 3");
        this.game.state.start('graphics');
    }

    onButton4 () {
        console.log ("pressed button 4");
        this.game.state.start('mask');
    }

    onButton5 () {
        console.log ("pressed button 5");
        this.game.state.start('tint');
    }

    onButton6 () {
        console.log ("pressed button 6");
        this.game.state.start('rect');
    }

    onButton7 () {
        console.log ("pressed button 7");
        this.game.state.start('circle');
    }

    onButton8 () {
        console.log ("pressed button 8");
        this.game.state.start('point');
    }

    onButton9 () {
        console.log ("pressed button 9");
        this.game.state.start('pixelperfect');
    }

    onButton10 () {
        console.log ("pressed button 10");
        this.game.state.start('basictext');
    }

    onButton11 () {
        console.log ("pressed button 11");
        this.game.state.start('bitmaptext');
    }

    onButton12 () {
        console.log ("pressed button 12");
        this.game.state.start('midterms');
    }

    update() {
        
    }

    _startGame () {
        // switch state to game
    	this.game.state.start('game');
    }

    test (a, b, c) {
        var a = 0;

        return a;
    }
}
