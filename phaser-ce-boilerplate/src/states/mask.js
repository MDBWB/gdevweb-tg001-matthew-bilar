class Mask extends Phaser.State {
    constructor() {
    	super();
    }

    preload() {
        this.game.load.image('dota', 'assets/dota.jpg');
    }

    create() {
        this.background = this.game.add.image(0,0,'dota');

        this.input.onDown.add(this._startMenu, this);

        this.createMask ();
    }   

    createMask () {
        var mask = this.game.add.graphics();

        mask.beginFill(0xffffff);
        mask.drawCircle(100, 100, 100);
        
        this.background.mask = mask;

        this.mask = mask;
    }

    update() {
        this.mask.x = this.game.input.x - 100;
	    this.mask.y = this.game.input.y - 100;
    }

    _startMenu () {
    	this.game.state.start('menu');
    }
}
