
// define phaser game
const game = new Phaser.Game(800, 600, Phaser.AUTO, 'game_canvas');

// add menu state
game.state.add('menu', new Menu());

game.state.add('intro', new Intro());

game.state.add('animation', new Animation());

game.state.add('graphics', new Graphics());

game.state.add('mask', new Mask());

game.state.add('tint', new ColorTint());

game.state.add('rect', new RectCollision());

game.state.add('circle', new CircleCollision());

game.state.add('point', new PointCollision());

game.state.add('pixelperfect', new PixelCollision());

game.state.add('basictext', new BasicText());

game.state.add('bitmaptext', new BitmapText());

game.state.add('midterms', new Midterms());

// initialize menu state
game.state.start('menu');
